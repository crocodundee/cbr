import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def dollarCurs(dollar):
    plt.title("Курс доллара США по данным ЦБР", color='green', fontsize=14)
    plt.ylabel("Курс", color='green')
    plt.xlabel("Изменение в течении года", color='green')
    plt.grid(True)
    plt.plot(dollar)
    plt.xticks(rotation=30)
    plt.show()


def getData(past, future):
    X = df[past][0:-1]
    y = df[future][0:-1]
    X_test = df[past][-1:]
    y_test = df[future][-1:]
    return X, y, X_test, y_test


def printAlgoResult(algo_name, result):
    plt.title(algo_name, color='green')
    plt.plot(result, label="predicted")
    plt.plot(df[future_columns].iloc[-1], label="real")
    plt.legend()
    plt.show()


def getPrediction(algo_name, reg, X_test, y_test):
    prediction = reg.predict(X_test)[0]
    diff = prediction - y_test
    error = np.linalg.norm(diff)
    print('Пoгрешность: {:.3}'.format(error))
    printAlgoResult(algo_name, prediction)


def linearRegreshion(past, future):
    algorithm_name = "Метод линейной регрессии"
    from sklearn.linear_model import LinearRegression
    print(algorithm_name)

    X, y, X_test, y_test = getData(past, future)
    reg = LinearRegression().fit(X, y)
    getPrediction(algorithm_name, reg, X_test, y_test)


def neighborsRegressor(past, future):
    algorithm_name = "Метод Ближайших соседей"
    from sklearn.neighbors import KNeighborsRegressor
    print(algorithm_name)

    X, y, X_test, y_test = getData(past, future)
    reg = KNeighborsRegressor(n_neighbors=5).fit(X, y)
    getPrediction(algorithm_name, reg, X_test, y_test)


def lassoAlgorithm(past, future):
    algorithm_name = "Алгоритм Лассо"
    from sklearn import linear_model

    print(algorithm_name)
    X, y, X_test, y_test = getData(past, future)
    reg = linear_model.LassoLars(alpha=.1).fit(X, y)
    getPrediction(algorithm_name, reg, X_test, y_test)


def neuralNetwork(past, future):
    algorithm_name = "Нейронная сеть"
    from sklearn import neural_network

    print(algorithm_name)
    X, y, X_test, y_test = getData(past, future)
    reg = neural_network.MLPRegressor(hidden_layer_sizes=(100,),
                                      activation='logistic',
                                      solver='lbfgs',
                                      alpha=0.0001,
                                      batch_size='auto',
                                      learning_rate='adaptive',
                                      learning_rate_init=0.00001,
                                      power_t=0.005,
                                      max_iter=10000,
                                      shuffle=True,
                                      random_state=20,
                                      tol=0.0001,
                                      verbose=False,
                                      warm_start=False,
                                      momentum=0.009,
                                      nesterovs_momentum=False,
                                      early_stopping=True,
                                      validation_fraction=0.01,
                                      beta_1=0.1,
                                      beta_2=0.111,
                                      epsilon=1e-09)
    reg = reg.fit(X, y)
    getPrediction(algorithm_name, reg, X_test, y_test)


if __name__ == "__main__":
    money = pd.read_csv("D:/python/cbr/money.csv")
    values = money['value']
    dollarCurs(values)
    past = 7 * 4
    future = 7 * 1
    df = list()

    for i in range(past, len(money) - future):
        part_of_values = values[(i - past):(i + future)]
        df.append(list(part_of_values))

    past_columns = [f"past_{i + 1}" for i in range(past)]
    future_columns = [f"future_{i + 1}" for i in range(future)]
    df = pd.DataFrame(df, columns=(past_columns + future_columns))

    linearRegreshion(past_columns, future_columns)
    neighborsRegressor(past_columns, future_columns)
    lassoAlgorithm(past_columns, future_columns)
    neuralNetwork(past_columns, future_columns)



